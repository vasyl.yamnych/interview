import Dependencies._

lazy val commonSettings = Seq(
  scalaVersion := "2.12.8",
  organization := "interview",
  scalacOptions ++= Seq(
    "-language:implicitConversions",
    "-language:higherKinds",
    "-language:existentials",
    "-language:postfixOps"
  )
)

lazy val interview = (project in file("."))
  .settings(
    commonSettings,
    name := "interview",
    version := "0.1",
    libraryDependencies ++= Seq(
      pureconfig,
      logback,
      scalaLogging,
      scalaTest
    )
  )
