import sbt._

object Dependencies {
  private val scalaTestVersion = "3.0.5"
  private val pureconfigVersion = "0.10.2"
  private val logbackVersion = "1.2.3"
  private val scalaLoggingVersion = "3.9.2"

  lazy val pureconfig = "com.github.pureconfig" %% "pureconfig" % pureconfigVersion
  lazy val logback = "ch.qos.logback" % "logback-classic" % logbackVersion
  lazy val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion
  lazy val scalaTest = "org.scalatest" %% "scalatest" % scalaTestVersion % Test
}
