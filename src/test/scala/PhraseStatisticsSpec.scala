import org.scalatest.FreeSpec

class PhraseStatisticsSpec extends FreeSpec {

  import PhraseStatistics._

  val sentences = List("sentence 1.sentenceLONGEST 2.sentence 2.", "sentence1 1.sentence2 2.sentence3 3.", " ", "")
  val sentencesCountResults = List(3, 3, 0, 0)
  val wordsCountResults = List(6, 6, 0, 0)
  val longestWordsLengthResults = List("sentenceLONGEST".length, "sentence1".length, 0, 0)
  val longestWordsResults = List(List("sentenceLONGEST"), List("sentence1", "sentence2", "sentence3"), List(), List())

  val phrase = "a bb ccc dddd 1.a bb ccc dddd 2.a ccc dddd 3.a ccc dddd 4.ccc dddd 5."
  val wordsOccurTheMostCheck = List((-1, List()),
                                    (0, List()),
                                    (1, List("ccc", "dddd")),
                                    (2, List("ccc", "dddd")),
                                    (3, List("ccc", "dddd", "a")),
                                    (4, List("ccc", "dddd", "a", "bb")))

  val uniqueWordsResult = Set("1", "2", "3", "4", "5")
  val uniquePercentageOfWordsResult = uniqueWords(phrase).length * 100.00 / wordsCount(phrase)
  val averageNumberOfWordsPerSentenceResult = wordsCount(phrase) * 1.0 / sentencesCount(phrase)
  val twoWordPhrasesOccurredTheMostResult = List((-1, Set()),
                                                 (0, Set()),
                                                 (1, Set("ccc dddd")),
                                                 (2, Set("ccc dddd", "bb ccc", "a ccc", "a bb")),
                                                 (3, Set("ccc dddd", "bb ccc", "a ccc", "a bb")))

  "sentences count" - {
    sentences.zip(sentencesCountResults).foreach { s =>
      s"\42${s._1}\42 has ${s._2} sentences" in {
        assertResult(s._2)(sentencesCount(s._1))
      }
    }
  }

  "words count" - {
    sentences.zip(wordsCountResults).foreach { s =>
      s"\42${s._1}\42 has ${s._2} words" in {
        assertResult(s._2)(wordsCount(s._1))
      }
    }
  }

  "the longest words length" - {
    sentences.zip(longestWordsLengthResults).foreach { s =>
      s"of \42${s._1}\42 is ${s._2}" in {
        assertResult(s._2)(longestWordsLength(s._1))
      }
    }
  }

  "the longest words" - {
    sentences.zip(longestWordsResults).foreach { s =>
      s"\42${s._1}\42 has the following ${s._2} longest words" in {
        assertResult(s._2)(longestWords(s._1))
      }
    }
  }

  "the most occurred" - {
    s"at \42 $phrase\42" - {
      wordsOccurTheMostCheck.foreach { s =>
        s"${s._1} words are ${s._2}" in {
          assertResult(s._2)(wordsOccurTheMost(s._1)(phrase))
        }
      }
    }
  }

  "unique words" - {
    s"at \42 $phrase\42 are $uniqueWordsResult" in {
      assertResult(uniqueWordsResult)(uniqueWords(phrase).toSet)
    }
  }

  "unique percentage of words" - {
    s"at \42 $phrase\42 is $uniquePercentageOfWordsResult" in {
      assertResult(uniquePercentageOfWordsResult)(uniquePercentageOfWords(phrase))
    }
  }

  "average number of words per sentence" - {
    s"at \42 $phrase\42 is $averageNumberOfWordsPerSentenceResult" in {
      assertResult(averageNumberOfWordsPerSentenceResult)(averageNumberOfWordsPerSentence(phrase))
    }
  }

  "the most occurred" - {
    s"at \42 $phrase\42" - {
      twoWordPhrasesOccurredTheMostResult.foreach { s =>
        s"${s._1} two-word phrases are ${s._2}" in {
          assertResult(s._2)(twoWordPhrasesOccurredTheMost(s._1)(phrase).toSet)
        }
      }
    }
  }
}
