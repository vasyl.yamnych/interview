object Main {
  def main(args: Array[String]): Unit = {
    val text = "lorem ipsum dolor sit amet consectetur lorem ipsum et mihi quoniam et adipiscing elit.sed quoniam et advesperascit et mihi ad villam revertendum est nunc quidem hactenus ex rebus enim timiditas non ex vocabulis nascitur.nummus in croesi divitiis obscuratur pars est tamen divitiarum.nam quibus rebus efficiuntur voluptates eae non sunt in potestate sapientis.hoc mihi cum tuo fratre convenit.qui ita affectus beatum esse numquam probabis duo reges constructio interrete.de hominibus dici non necesse est.eam si varietatem diceres intellegerem ut etiam non dicente te intellego parvi enim primo ortu sic iacent tamquam omnino sine animo sint.ea possunt paria non esse.quamquam tu hanc copiosiorem etiam soles dicere.de quibus cupio scire quid sentias.universa enim illorum ratione cum tota vestra confligendum puto.ut nemo dubitet eorum omnia officia quo spectare quid sequi quid fugere debeant nunc vero a primo quidem mirabiliter occulta natura est nec perspici nec cognosci potest.videmusne ut pueri ne verberibus quidem a contemplandis rebus perquirendisque deterreantur sunt enim prima elementa naturae quibus auctis virtutis quasi germen efficitur.nam ut sint illa vendibiliora haec uberiora certe sunt.cur deinde metrodori liberos commendas.mihi inquam qui te id ipsum rogavi nam adhuc meo fortasse vitio quid ego quaeram non perspicis.quibus ego vehementer assentior.cur iustitia laudatur mihi enim satis est ipsis non satis.quid est enim aliud esse versutum nobis heracleotes ille dionysius flagitiose descivisse videtur a stoicis propter oculorum dolorem.diodorus eius auditor adiungit ad honestatem vacuitatem doloris.nos quidem virtutes sic natae sumus ut tibi serviremus aliud negotii nihil habemus."
    println(s"$text")
    println("===== ===== ===== ===== ===== ===== =====")
    println("Five words occur the most in the text are (it could be more then five because of words with the same occurrence is not skipped)")
    println(s"${PhraseStatistics.wordsOccurTheMost(5)(text).mkString(",")}")
    println("===== ===== ===== ===== ===== ===== =====")
    println("Percentage of the unique words is")
    println(s"${PhraseStatistics.uniquePercentageOfWords(text)}")
    println("===== ===== ===== ===== ===== ===== =====")
    println("Average number of words per sentence is")
    println(s"${PhraseStatistics.averageNumberOfWordsPerSentence(text)}")
    println("===== ===== ===== ===== ===== ===== =====")
    println("Three two-word phrases occur the most in the text are (it could be more then three because of two-word phrases with the same occurrence is not skipped)")
    println(s"${PhraseStatistics.twoWordPhrasesOccurredTheMost(3)(text).mkString(",")}")
  }
}
