object PhraseStatistics {

  def sentencesCount(phrase: String): Int =
    phrase.filter(_ == '.').length

  def wordsCount(phrase: String): Int =
    phrase.trim.filter(c => c == '.' || c == ' ').length

  def longestWordsLength(phrase: String): Int = {
    words(phrase).foldLeft(0)((acc, w) => if (w.length > acc) w.length else acc)
  }

  def longestWords(phrase: String): List[String] = {
    if (phrase.trim.isEmpty) List()
    else {
      val pairs = words(phrase).map(w => (w, w.length))
      val maxLength: Int = pairs.foldLeft(0)((acc, p) => if (p._2 > acc) p._2 else acc)
      pairs.groupBy(_._2).get(maxLength) match {
        case None    => List()
        case Some(v) => v.map(_._1).toList
      }
    }
  }

  /*
   * To show all words with the same level of occurrence even though a limit is exceeded is a developer's assumption.
   * Another option is to show at most a limit amount of words even though several words with the same occurrence will be skipped.
   * This should be clarified with a customer.
   *
   **/
  def wordsOccurTheMost(limit: Int)(phrase: String): Array[String] = {
    if (limit <= 0) Array()
    else {
      val sortedByOccurrenceWords = words(phrase)
        .groupBy(w => w)
        .mapValues(_.length)
        .groupBy(_._2)
        .mapValues(_.keys.toArray)
        .toArray
        .sortBy(_._1)(Ordering[Int].reverse)
      var nWords = 0
      sortedByOccurrenceWords
        .takeWhile(p => { val b = nWords < limit; nWords += p._2.length; b })
        .flatMap(_._2)
    }
  }

  def uniqueWords(phrase: String): Array[String] =
    words(phrase)
      .groupBy(c => c)
      .mapValues(_.length)
      .filter(_._2 == 1)
      .keys
      .toArray

  /*
   * Calculation formula "uniqueWords / wordsCount * 100" is a developer's assumption.
   * Another option is "uniqueWords / (nonUniqueWords + uniqueWords)".
   * This should be clarified with a customer.
   *
   **/
  def uniquePercentageOfWords(phrase: String): Double = {
    val wc = wordsCount(phrase)
    if (wc > 0)
      uniqueWords(phrase).length * 100.0 / wc
    else 0
  }

  /*
   * Calculation formula "wordsCount / sentencesCount" is a developer's assumption.
   * Another option is "avg ( wordsCount per sentence )".
   * This should be clarified with a customer.
   *
   **/
  def averageNumberOfWordsPerSentence(phrase: String): Double = {
    val sc = sentencesCount(phrase)
    if (sc > 0)
      wordsCount(phrase) * 1.0 / sc
    else 0
  }

  /*
   * To show all two-words phrases with the same level of occurrence even though a limit is exceeded is a developer's assumption.
   * Another option is to show at most a limit amount of two-words phrases even though several two-words phrases with the same occurrence will be skipped.
   * This should be clarified with a customer.
   *
   **/
  def twoWordPhrasesOccurredTheMost(limit: Int)(phrase: String): Array[String] = {
    if (limit <= 0) Array()
    else {
      val twoWordsPhrases =
        sentences(phrase)
          .map(words)
          .flatMap(arr => arr.dropRight(1).zip(arr.drop(1)))
          .map(p => s"${p._1} ${p._2}")
      val sortedByOccurrenceOfTwoWords = twoWordsPhrases
        .groupBy(tw => tw)
        .mapValues(_.length)
        .groupBy(_._2)
        .mapValues(_.keys.toArray)
        .toArray
        .sortBy(_._1)(Ordering[Int].reverse)
      var nWords = 0
      sortedByOccurrenceOfTwoWords
        .takeWhile(p => { val b = nWords < limit; nWords += p._2.length; b })
        .flatMap(_._2)
    }
  }

  private def words(phrase: String): Array[String] =
    phrase.trim.split(Array(' ', '.'))

  private def sentences(phrase: String): Array[String] =
    phrase.trim.split('.')
}
